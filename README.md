# Facets missing merge

Provides a facet processor that allows the missing facet item to be merged into
another item.

## Example

If you have a facet with the following items:

- Cat
- Dog
- Lizard

With the facet "missing" option enabled, an extra item with a configurable title
is added for items that do not have any of those values and your list becomes.

- Cat
- Dog
- Lizard
- None

With this processor enabled, you can merge the missing facet item (None) onto an
existing item (in this example, Cat is the target).

- Cat (or None)
- Dog
- Lizard


## Requirements

- [Facets](https://www.drupal.org/project/facets)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

When enabling the processor for a facet ensure that it is ordered to execute
*before* the URL processor in the build phase. This is because the processor
modifies the missing filter parameters on a facet item.


## Maintainers

- Eric Smith - [ericgsmith](https://www.drupal.org/u/ericgsmith)
