<?php

namespace Drupal\facets_missing_merge\Plugin\facets\processor;

use Drupal\Core\Cache\UnchangingCacheableDependencyTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;

/**
 * Provides a processor that excludes specified items.
 *
 * @FacetsProcessor(
 *   id = "missing_item_merge",
 *   label = @Translation("Merge missing item"),
 *   description = @Translation("Merges the missing facet item onto an item depending on their raw or display value (such as node IDs or titles)."),
 *   stages = {
 *     "build" = 5
 *   }
 * )
 */
class MissingItemMergeProcessor extends ProcessorPluginBase implements BuildProcessorInterface {

  use UnchangingCacheableDependencyTrait;

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    if (!$facet->isMissing()) {
      return $results;
    }

    $missing_position = $this->getMissingFacetItemKey($results);
    $target_position = $this->getTargetFacetItemKey($results);

    if (is_null($missing_position) || is_null($target_position)) {
      return $results;
    }

    $missing_facet_result = $results[$missing_position];
    $target_facet_result = $results[$target_position];
    // Remove the value from the missing filters.
    $excluded_filters = array_filter($missing_facet_result->getMissingFilters(), function ($filter_value) use ($target_facet_result) {
      return $filter_value != $target_facet_result->getRawValue();
    });
    $missing_facet_result->setMissingFilters(array_values($excluded_filters));
    // Merge the count into the missing count.
    $missing_facet_result->setCount($missing_facet_result->getCount() + $target_facet_result->getCount());

    // Merge label.
    $missing_label = $missing_facet_result->getDisplayValue();
    $target_label = $target_facet_result->getDisplayValue();
    $missing_facet_result->setDisplayValue((string) $this->t('@target (or @missing)', [
      '@target' => $target_label,
      '@missing' => $missing_label,
    ]));

    // Replace the target with the missing item.
    $results[$target_position] = $missing_facet_result;
    unset($results[$missing_position]);

    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $config = $this->getConfiguration();

    $build['target_item'] = [
      '#title' => $this->t('Target item'),
      '#type' => 'textfield',
      '#default_value' => $config['target_item'],
      '#description' => $this->t('Title or value that should be used as the target to merge the missing item onto.'),
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'target_item' => '',
    ];
  }

  /**
   * Get the missing facet item key.
   *
   * @param \Drupal\facets\Result\ResultInterface[] $results
   *   List of facet item results.
   *
   * @return string|int|null
   *   The key of the facet item result that represents the missing facet item.
   */
  protected function getMissingFacetItemKey(array $results) {
    /** @var \Drupal\facets\Result\ResultInterface $result */
    foreach ($results as $key => $result) {
      if ($result->isMissing()) {
        return $key;
      }
    }

    return NULL;
  }

  /**
   * Get the target facet item key.
   *
   * @param \Drupal\facets\Result\ResultInterface[] $results
   *   List of facet item results.
   *
   * @return string|int|null
   *   The key of the facet item result that represents the target facet item.
   */
  protected function getTargetFacetItemKey(array $results) {
    $config = $this->getConfiguration();
    $target_item = trim($config['target_item']);
    /** @var \Drupal\facets\Result\ResultInterface $result */
    foreach ($results as $key => $result) {
      if ($result->getRawValue() == $target_item || $result->getDisplayValue() == $target_item) {
        return $key;
      }
    }

    return NULL;
  }

}
